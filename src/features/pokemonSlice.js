import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";


export const fetchPokemon = createAsyncThunk(
    "pokemon/fetchPokemon",
    () => {
        return axios
            .get("https://pokeapi.co/api/v2/pokemon?limit=100")
            .then((res) => res.data.results.map(pokemon => axios.get(pokemon.url)))
            .then((promises) => Promise.all(promises))
            .then((results) => results.map((result) => result.data));
    }
);

const pokemonSlice = createSlice({
    name: "pokemon",
    initialState: {
        pokemons: [],
        status: null,
    },
    reducers: {},
    extraReducers: (builder) => {
        builder
        .addCase(fetchPokemon.pending, (state) => {
                state.status = "loading";
            })
            .addCase(fetchPokemon.fulfilled, (state, action) => {
                state.status = "succeeded";
                state.pokemons = action.payload;
            })
            .addCase(fetchPokemon.rejected, (state) => {
                state.status = "failed";
            });
    }
});

export default pokemonSlice.reducer;
