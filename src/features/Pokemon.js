import React, {useRef} from "react";

import { useSelector, useDispatch } from "react-redux";
import { fetchPokemon } from "../features/pokemonSlice";

function Pokemon() {
  const dispatch = useDispatch();
  const pokemon = useSelector((state) => state.pokemon);
  const status = useSelector((state) => state.pokemon.status);
  const pokemonName = useRef();

  return (
    <div>
      <input
        type="text"
        ref={pokemonName}

      />
        <button onClick={() => dispatch(fetchPokemon())}>Search</button>
      {status === "loading" && <div>Loading...</div>}
      {status === "succeeded" && (
        <div>
          <img src={pokemon.sprites.front_default} alt="pokemon" />
          <div>{pokemon.name}</div>
          <div>
            {pokemon.types.map((type) => type.type.name).join(", ")}
          </div>
          <div>
            {pokemon.abilities.map((ability) => ability.ability.name).join(", ")}
          </div>
        </div>
      )}
        {status === "failed" && (<div>Failed to find pokemon</div>)}
    </div>
  );
}

export default Pokemon;