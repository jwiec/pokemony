import {useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {FormControl, Input, Table} from "@mui/material";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import TableBody from "@mui/material/TableBody";



function ComparisonPage() {
    const pokemon = useSelector((state) => state.pokemon);
    const selectedPokemon = useSelector((state) => state.compare);

    const pokemon1 = pokemon.pokemons.find((pokemon) => pokemon.id === selectedPokemon.compareItems[0]);
    const pokemon2 = pokemon.pokemons.find((pokemon) => pokemon.id === selectedPokemon.compareItems[1]);

    console.log(pokemon1);
    console.log(pokemon2);

    return (
        <div>
            {/*Pokemon not selected*/}
            {selectedPokemon.compareItems.length !== 2 && (
                <div>
                    <h1>Please select 2 pokemon to compare</h1>
                    <Link to="/">Go back to pokemon list</Link>
                </div>
            )}

            {/*Pokemon selected*/}
            {selectedPokemon.compareItems.length === 2 && (
                <div>
                    <h1>Comparison Page</h1>
                    <div>
                        <Table>
                            <TableBody>
                            <TableRow>
                                <TableCell>
                                    <img src={pokemon1?.sprites.front_default || "No image"} alt="pokemon" />
                                </TableCell>
                                <TableCell>
                                    <img src={pokemon2?.sprites.front_default || "No image"} alt="pokemon" />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <FormControl>
                                        <p>Name</p>
                                        <Input
                                            value={pokemon1?.name || ""}
                                            readOnly
                                        />
                                    </FormControl>
                                </TableCell>
                                <TableCell>
                                    <FormControl>
                                        <p>Name</p>
                                        <Input
                                            value={pokemon2?.name || ""}
                                            readOnly
                                        />
                                    </FormControl>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <FormControl>
                                        <p>Height</p>
                                        <Input
                                            value={pokemon1?.height || ""}
                                            readOnly
                                        />
                                    </FormControl>
                                </TableCell>
                                <TableCell>
                                    <FormControl>
                                        <p>Height</p>
                                        <Input
                                            value={pokemon2?.height || ""}
                                            readOnly
                                        />
                                    </FormControl>
                                </TableCell>
                            </TableRow>
                            </TableBody>
                        </Table>
                    </div>
                    <div>
                    </div>
                    <Link to="/">Go back to pokemon list</Link>
                </div>
            )}

        </div>
    );
}

export default ComparisonPage;