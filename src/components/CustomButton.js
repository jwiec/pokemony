import { styled } from "@mui/material";
import Button from "@mui/material/Button";
import { green } from "@mui/material/colors";

const CustomButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(green[500]),
    backgroundColor: "none",
    borderColor: green[50],
    textTransform: "none",
    fontSize: 13,
    "&:hover": {
        backgroundColor: green[200],
        borderColor: green[200],
    },
}));
export default CustomButton;


