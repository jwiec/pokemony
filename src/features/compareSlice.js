import { createSlice } from "@reduxjs/toolkit";
import {createAction} from "@reduxjs/toolkit";

const initialState = {
    compareItems: [],
    compareTotalQuantity: 0,
};

const quantityFull = createAction("quantityFull");

const compareSlice = createSlice({
    name: "compare",
    initialState,
    reducers: {
        addToCompare: (state, action) => {
            //if (state.compareItems.includes(action.payload) || state.compareTotalQuantity >= 2) return;
            state.compareItems.push(action.payload);
            state.compareTotalQuantity++;
        },
        clearCompare: (state) => {
            state.compareItems = [];
            state.compareTotalQuantity = 0;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(quantityFull, (state) => {
            state.compareTotalQuantity = 2;
        });

    }
});

export const { addToCompare, clearCompare } = compareSlice.actions;

export default compareSlice.reducer;