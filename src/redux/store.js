import { configureStore } from '@reduxjs/toolkit'
import pokemonReducer from '../features/pokemonSlice'
import compareReducer from '../features/compareSlice'

export const store = configureStore({
    reducer: {
        pokemon: pokemonReducer,
        compare: compareReducer,
    },
})