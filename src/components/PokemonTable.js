import {useCallback, useMemo, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import Table from "./Table";
import CssBaseline from '@mui/material/CssBaseline'
import DetailModal from "./DetailModal";
import { Link } from "react-router-dom";
import CustomButton from "./CustomButton";
import {addToCompare, clearCompare} from "../features/compareSlice";


function PokemonTable() {

    const pokemon = useSelector((state) => state.pokemon);
    const selectedPokemon = useSelector((state) => state.compare);
    const dispatch = useDispatch();


    const handleAddToComparison = useCallback((pokemon) => {
        if (selectedPokemon.compareItems.length < 2 && !selectedPokemon.compareItems.includes(pokemon)) {
            dispatch(addToCompare(pokemon));
        }
        else {
            alert("You can only compare 2 different pokemon");
        }
    }, [selectedPokemon], dispatch);

    console.log(selectedPokemon.compareItems);

    const handleClearComparison = () => {
        dispatch(clearCompare());
    }

    const columns = useMemo(
        () => [
            {
                Header: "Image",
                accessor: "image",
                Cell: ({ row }) => {
                    return (
                        <img src={row.original.sprites.front_default} alt="pokemon" />
                    )
                }
            },
            {
                Header: "Name",
                accessor: "name",
            },
            {
                Header: "Type",
                accessor: "types",
                Cell: ({ value }) => {
                    return (
                        <>
                            <p>{value[0].type.name}</p>
                            {(value.length > 1 &&
                                <DetailModal name="Types" title={value.length - 1} allAtributes={value.map((type) => type.type.name).join(", ")}/>
                            )}
                        </>
                    );
                }
            },
            {
                Header: "Abilities",
                accessor: "abilities",
                Cell: ({ value }) => {
                    return (
                        <>
                            <p>{value[0].ability.name}</p>
                            {(value.length > 1 &&
                                <DetailModal name="Abilities" title={value.length - 1} allAtributes={value.map((ability) => ability.ability.name).join(", ")}/>
                            )}
                        </>
                    );
                }
            },
            {
                Header: "Action",
                accessor: "id",
                Cell: ({ value }) => {

                    return (
                        <>
                            <CustomButton component={Link} to={`/details/${value}`} >Details</CustomButton>
                            {selectedPokemon.compareTotalQuantity < 2 &&
                            <CustomButton onClick={() => handleAddToComparison(value)}>Compare</CustomButton>}
                            {selectedPokemon.compareTotalQuantity === 2 &&
                            <CustomButton disabled={true}>Compare</CustomButton>}

                        </>
                    );
                }
            },
        ],
        [handleAddToComparison]
    );

    console.log('rerendering');
    const pokemon1 = pokemon.pokemons.find((pokemon) => pokemon.id === selectedPokemon.compareItems[0]);
    const pokemon2 = pokemon.pokemons.find((pokemon) => pokemon.id === selectedPokemon.compareItems[1]);

    return (
        <div>
        <div>
            <p>Compared pokemons:</p>
            {selectedPokemon.compareTotalQuantity === 1 && <p>{pokemon1?.name}</p>}
            {selectedPokemon.compareTotalQuantity === 2 &&
            <p>{pokemon1?.name} and {(pokemon2?.name)}</p>}
        </div>
            <div>
                {selectedPokemon.compareTotalQuantity === 0 &&
                <CustomButton disabled={true}>Clear pokemon to compare</CustomButton>}
                {selectedPokemon.compareTotalQuantity > 0 &&
                <CustomButton onClick={handleClearComparison}>Clear pokemon to compare</CustomButton>}
                {selectedPokemon.compareTotalQuantity < 2 &&
                <CustomButton disabled={true}>Compare</CustomButton>}
                {selectedPokemon.compareTotalQuantity === 2 &&
                    <>
                        <CustomButton component={Link} to="/comparison">Compare</CustomButton>
                    </>
                }
            </div>
            {pokemon.loading && <div>Loading...</div>}
            {pokemon.error && <div>Failed to fetch pokemons</div>}
            <CssBaseline/>
            {pokemon.pokemons && (
                <Table columns={columns} data={pokemon.pokemons} />
            )}
        </div>

    );
}

export default PokemonTable;