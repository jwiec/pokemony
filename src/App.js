import "./App.css";
import PokemonTable from "./components/PokemonTable";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {fetchPokemon} from "./features/pokemonSlice";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import DetailsForm from "./components/DetailsForm";
import ComparisonPage from "./components/ComparisonPage";


function App() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPokemon());
    }, []);

  return (
    <div className="App">
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<PokemonTable />} />
                <Route path="/details/:pokemonId" element={<DetailsForm />} />
                <Route path="/comparison" element={<ComparisonPage />} />
            </Routes>
        </BrowserRouter>
    </div>
  );
}

export default App;
