import {useSelector} from "react-redux";
import {useParams} from "react-router";
import {FormControl, Input} from "@mui/material";
import CustomButton from "./CustomButton";
import {Link} from "react-router-dom";
import {Table} from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";


function DetailsForm() {
    const pokemonDetail = useSelector((state) => state.pokemon);
    const { pokemonId } = useParams();

    const pokemon = pokemonDetail.pokemons.find((pokemon) => String(pokemon.id) === pokemonId);

    return (
        <div>
            <h1>Details</h1>
            <CustomButton component={Link} to="/" >Go back to pokemon list</CustomButton>
            <div>
                <Table>
                    <TableBody>
                    <TableRow>
                        <img src={pokemon?.sprites.front_default || "No image"} alt="pokemon" />
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Name</p>
                            <Input
                                value={pokemon?.name || ""}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Types</p>
                            <Input
                                value={pokemon?.types.map((type) => type.type.name).join(", ") || ""}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Abilities</p>
                            <Input
                                value={pokemon?.abilities.map((ability) => ability.ability.name).join(", ") || ''}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Height</p>
                            <Input
                                value={pokemon?.height || 0}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Weight</p>
                            <Input
                                value={pokemon?.weight || ""}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Base experience</p>
                            <Input
                                value={pokemon?.base_experience || 0}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Order</p>
                            <Input
                                value={pokemon?.order || "N/A"}
                                readOnly
                            />
                        </FormControl>
                    </TableRow>
                    <TableRow>
                        <FormControl>
                            <p>Game Indices</p>
                            <Input
                                value={pokemon?.game_indices.map((game) => game.version.name).join(", ") || "No game indices"}
                                readOnly
                                multiline={true}
                            />
                        </FormControl>
                    </TableRow>
                    </TableBody>
                </Table>
            </div>
        </div>
    );
}

export default DetailsForm;